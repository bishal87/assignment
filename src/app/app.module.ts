import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { RouterModule } from '@angular/router';
import { FixturesComponent } from './components/fixtures/fixtures.component';
import { StandingsComponent } from './components/standings/standings.component';
import { HttpClientModule } from '@angular/common/http';
import { DataService } from './data.service';
import { ResultsComponent } from './components/results/results.component';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FixturesComponent,
    StandingsComponent,
    ResultsComponent
  ],
  imports: [
    
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule.forRoot([
      {path:'components/home', component : HomeComponent },
      {path:'components/fixtures', component: FixturesComponent},
      {path: 'components/standings', component: StandingsComponent},
      {path: 'components/results', component: ResultsComponent}

    ])
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
