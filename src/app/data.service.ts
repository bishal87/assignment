import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';


const httpOptionsA = {
	headers: new HttpHeaders ({
		'X-Auth-Token': '256664ba6f82415c923ab2bf76f516c2'
	})
  };
  
@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { 
  }


  getTable (leagueID : string) : Observable <Object[]>
  { // pass in the league ID


  		var urlPrefix = "http://api.football-data.org/v1/competitions/";
		  var urlSuffix = "/leagueTable";
		
		return this.http.get<Object[]>(urlPrefix+leagueID+urlSuffix, httpOptionsA); 
		
  		
  }
}
