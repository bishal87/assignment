export class result{
    counts: number;
    filters: Filters;
    matches: Matches[];

}

export class Filters{
    permission: string;
    status: Status[];
    limit: number;
}

export class Status{
    

}

export class Matches{
    score: Score;
    homeTeam: {
        name: string;
    }
    string: {
        name: string;
    }
           
}

export class Score{
    fullTime: FullTime;
}

export class FullTime{
    homeTeam: number;
    awayTeam: number;
}