import { of } from 'rxjs';


export interface IStandings{
	filters: Filters;
	competition: Competition;
	season: Season; 
	standings: Standing[];
}

export class Standings implements IStandings{
	filters: Filters;
	competition: Competition;
	season: Season; 
	standings: Standing[];
	copy(source: IStandings): void {
		if(!source){
			return;
		}
		this.filters = source.filters;
		this.competition = source.competition;
		this.season = source.season;
		this.standings = source.standings;

	}
}

export class Filters{

}
export class Competition{
	id: number;
	area: Area;
	name: string;
	code: string;
	plan: string;
	lastUpdated: string;

}
export class Area{
	id: number;
	name: string;
}

export class Season{
	id: number;
	startDate: string;
	endDate: string;
	currentMatchday: number;
	winner: string;
}
export class Standing{
	stage: String;
	type: String;
	group: String;
	table: Table[];
	
}

export class Table{
	position: number;
	team: Team;
	playedGames: number;
	won: number;
	draw: number;
	loss: number;
	points: number;
	goalsFor: number;
	goalAgainst: number;
	goalDifference: number;

}

export class Team{
	id: number;
	name: string;
	crestUrl: string;
}
