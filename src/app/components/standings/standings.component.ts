import { Component, OnInit } from '@angular/core';
import { getLocaleDateFormat } from '@angular/common';
import { Standings, IStandings, Table } from '../../standings';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-standings',
  templateUrl: './standings.component.html',
  styleUrls: ['./standings.component.css']
})
export class StandingsComponent implements OnInit {
  standings: Standings;
  table: Table[] = [];

  constructor(private http: HttpClient) { }

  ngOnInit() {

    this.getStandings();
  }

  getStandings() {
    let headers = new HttpHeaders({ 'X-Auth-Token': '256664ba6f82415c923ab2bf76f516c2' });

    this.http.get('http://api.football-data.org/v2/competitions/BSA/standings', { headers }).subscribe(data => {
      this.standings = new Standings;
      let matchData = <IStandings>data;
      this.standings.copy(matchData);

      this.table = this.standings.standings[0].table;

    });

  }
}

